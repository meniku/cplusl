#include "Enemy.h"
#include "World.h"
#include "GameScene.h"

Enemy::Enemy() : PhysicalSprite("Enemy")
{
}


Enemy::~Enemy()
{
}

void Enemy::enable()
{
	this->sound = getWorld()->resources->getSound("hit.wav");
	this->killSound = getWorld()->resources->getSound("kill.wav");
	this->stayInBounds = true;
	loadSprite("Enemy.bmp");
	particleSystem.setParent(this);
	particleSystem.loadFromConfig("particles_enemy");
	particleSystem.enable();
	particlesMinAngle = particleSystem.emitMinAngle;
	particlesMaxAngle = particleSystem.emitMaxAngle;
}

void Enemy::disable()
{
	this->stayInBounds = true;
	particleSystem.disable();
	particleSystem.setParent(NULL);
}

void Enemy::update(float deltaTime)
{
	PhysicalSprite::update(deltaTime);
	if (this->isOffScreen() && this->isEdgeHit()) {
		cout << "Removing Off Screen Enemy at " << position.x << " " << position.y << endl;
		this->disable();
        this->markForDeletion();
		this->setParent(NULL);
		return;
	}
	if (isDead()) {
		isDeadSince += deltaTime;
		if (isDeadSince > 1) {
			this->disable();
            this->markForDeletion();
			this->setParent(NULL);
			return;
		}
	}
}

void Enemy::hit(Vector2<float> dir, float strength)
{

	// TODO use proper Hitpoint model
	float newScale = this->scale.x - (1.0f + strength * 2.0f);
	if (newScale < 1) {
		particleSystem.emitMinAngle = -180.f * DEG_TO_RAD;
		particleSystem.emitMaxAngle = +180.f * DEG_TO_RAD;
		particleSystem.position = this->position;
		particleSystem.start();
		isDeadSince = 0;
		this->collisionEnabled = false;
		this->visible = false;
		((GameScene *) this->findParentWithName("GameScene"))->increaseScore();
		Mix_PlayChannel(0, this->killSound.sound, 0);
		return;
	}
	Mix_PlayChannel(0, this->sound.sound, 0);

	this->scale.xy(newScale);
	particleSystem.position = this->position;
	particleSystem.emitMinAngle = particlesMinAngle + dir.Angle();
	particleSystem.emitMaxAngle = particlesMaxAngle + dir.Angle();
	particleSystem.start();
}

bool Enemy::isDead() {
	return isDeadSince >= 0;
}
