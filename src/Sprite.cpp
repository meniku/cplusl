#include "PhysicalSprite.h"
#include "Sprite.h"
#include "World.h"

Sprite::Sprite(string name) : Entity(name)
{
}


Sprite::~Sprite()
{
}


void Sprite::loadSprite(string path)
{
	texture = this->getWorld()->resources->getTexture(path);
	spriteSize.x = this->getWorld()->pixelToWorldUnits(texture.size.x);
	spriteSize.y = this->getWorld()->pixelToWorldUnits(texture.size.y);
}

void Sprite::render(SDL_Renderer *renderer)
{
	if (!this->texture.valid) {
		return;
	}
	if (!visible) {
		return;
	}

	World *world = this->getWorld();

	SDL_Rect DestR, SourceR;

	Vector2<int> scaledWidth(0,0);
	scaledWidth.x = (int)round(this->texture.size.x * scale.x);
	scaledWidth.y = (int)round(this->texture.size.y * scale.y);

	Vector2<int> targetPoint = world->worldPointToScreenPoint(position);

	DestR.x = targetPoint.x - scaledWidth.x / 2;
	DestR.y = targetPoint.y - scaledWidth.y / 2;
	DestR.w = scaledWidth.x;
	DestR.h = scaledWidth.y;

	SourceR.x = 0;
	SourceR.y = 0;
	SourceR.w = this->texture.size.x;
	SourceR.h = this->texture.size.y;

	SDL_RenderCopyEx(renderer, texture.texture, &SourceR, &DestR, rotation * RAD_TO_DEG, NULL, SDL_FLIP_NONE);
}

Vector2<float> Sprite::size() const {
	return Vector2<float>(
		this->scale.x * this->spriteSize.x,
		this->scale.y * this->spriteSize.y
	);
}