#include "LuaConfig.h"

#include <iostream>
#include <sstream>

using namespace std;

class ParseException : public LuaConfigException
{
private: 
	const char *error;
public:
	ParseException(const char *error) :error(error) {
	}

	virtual const char* what() const throw()
	{
		return error;
	}
};

class RuntimeException : public LuaConfigException
{
private:
	string error;
public:
	RuntimeException(string error) :error(error) {
	}
  virtual ~RuntimeException() throw() {}

	virtual const char* what() const throw()
	{
		return error.c_str();
	}
};

LuaConfig::LuaConfig()
{
	this->L = luaL_newstate();
	luaL_openlibs(this->L);
	if (luaL_loadfile(this->L, "config.lua") || lua_pcall(this->L, 0, 0, 0)) {
		throw ParseException(lua_tostring(L, -1));
	}
}

int LuaConfig::getInt(char name[]) 
{
	lua_getglobal(this->L, name);

	if (!lua_isinteger(L, -1)) {
		lua_pop(L, 1);
		std::ostringstream stringStream;
		stringStream << "the global variable is not a int: " << name;
		throw RuntimeException(stringStream.str());
	}

	int i = lua_tointeger(L, -1);
	lua_pop(L, 1);
	return i;
}

float LuaConfig::getFloat(char name[])
{
	lua_getglobal(this->L, name);

	if (!lua_isnumber(L, -1)) {
		lua_pop(L, 1);
		std::ostringstream stringStream;
		stringStream << "the global variable is not a float: " << name;
		throw RuntimeException(stringStream.str());
	}

	float f = lua_tonumber(L, -1);
	lua_pop(L, 1);
	return f;
}

std::string LuaConfig::getString(char name[])
{
	lua_getglobal(this->L, name);

	if (!lua_isstring(L, -1)) {
		lua_pop(L, 1);
		std::ostringstream stringStream;
		stringStream << "the global variable is not a string";
		throw RuntimeException(stringStream.str());
	}

	size_t len;
	const char* cstr = lua_tolstring(L, -1, &len);
	lua_pop(L, 1);
	return std::string(cstr, len);
}

std::string LuaConfig::getString(char section[], char entry[])
{
	lua_getglobal(L, section);
	lua_getfield(L, -1, entry);

	if (!lua_isstring(L, -1)) {
		lua_pop(L, 2);
		std::ostringstream stringStream;
		stringStream << "the global variable is not a string";
		throw RuntimeException(stringStream.str());
	}

	size_t len;
	const char* cstr = lua_tolstring(L, -1, &len);
	lua_pop(L, 2);
	return std::string(cstr, len);
}

float LuaConfig::getFloat(const char section[], const char entry[])
{
	lua_getglobal(L, section);
	lua_getfield(L, -1, entry);

	if (!lua_isnumber(L, -1)) {
		lua_pop(L, 2);
		std::ostringstream stringStream;
		stringStream << "the table variable is not a number: " << entry;
		throw RuntimeException(stringStream.str());
	}

	float f= lua_tonumber(L, -1);
	lua_pop(L, 2);
	return f;
}


void LuaConfig::call(char table[], char function[], float arg1)
{
	lua_getglobal(L, table);
	lua_getfield(L, -1, function);

	lua_pushnumber(L, arg1);
	int error;

	if ((error = lua_pcall(L, 1, 0, 0))) {
		std::ostringstream stringStream;
		stringStream << "call " << function  << " error : " << lua_tostring(L, -1);
		lua_pop(L, 1);
		throw RuntimeException(stringStream.str());
	}
	lua_pop(L, 1);
}


void LuaConfig::call(char table[], char function[], float arg1, float arg2)
{
	lua_getglobal(L, table);
	lua_getfield(L, -1, function);

	lua_pushnumber(L, arg1);
	lua_pushnumber(L, arg2);
	int error;

	if ((error = lua_pcall(L, 2, 0, 0))) {
		std::ostringstream stringStream;
		stringStream << "call " << function << " error : " << lua_tostring(L, -1);
		lua_pop(L, 1);
		throw RuntimeException(stringStream.str());
		return;
	}
	lua_pop(L, 1);
}

void LuaConfig::call(char table[], char function[], float arg1, int arg2)
{
	lua_getglobal(L, table);
	lua_getfield(L, -1, function);

	lua_pushnumber(L, arg1);
	lua_pushnumber(L, arg2);
	int error;

	if ((error = lua_pcall(L, 2, 0, 0))) {
		std::ostringstream stringStream;
		stringStream << "call " << function << " error : " << lua_tostring(L, -1);
		lua_pop(L, 1);
		throw RuntimeException(stringStream.str());
		return;
	}
	lua_pop(L, 1);
}


float LuaConfig::callFloat(char table[], char function[], float arg1)
{
	lua_getglobal(L, table);
	lua_getfield(L, -1, function);

	lua_pushnumber(L, arg1);
	int error;

	if ((error = lua_pcall(L, 1, 1, 0))) {
		std::ostringstream stringStream;
		stringStream << "call " << function << " error : " << lua_tostring(L, -1);
		lua_pop(L, 2);
		throw RuntimeException(stringStream.str());
	}
	float f = lua_tonumber(L, -1);
	lua_pop(L, 2);
	return f;
}


float LuaConfig::callFloat(char table[], char function[], float arg1, float arg2)
{
	lua_getglobal(L, table);
	lua_getfield(L, -1, function);

	lua_pushnumber(L, arg1);
	lua_pushnumber(L, arg2);
	int error;

	if ((error = lua_pcall(L, 2, 1, 0))) {
		std::ostringstream stringStream;
		stringStream << "call " << function << " error : " << lua_tostring(L, -1);
		lua_pop(L, 2);
		throw RuntimeException(stringStream.str());

	}
	float f = lua_tonumber(L, -1);
	lua_pop(L, 2);
	return f;
}

int LuaConfig::getInt(char section[],  char entry[])
{
	lua_getglobal(L, section);
	lua_getfield(L, -1, entry);

	if (!lua_isinteger(L, -1)) {
		lua_pop(L, 2);
		std::ostringstream stringStream;
		stringStream << "the table variable is not a integer: " << entry;
		throw RuntimeException(stringStream.str());
	}
	int i= lua_tointeger(L, -1);
	lua_pop(L, 2);
	return i;
}


bool LuaConfig::getBool(char section[], char entry[])
{
	lua_getglobal(L, section);
	lua_getfield(L, -1, entry);

	if (!lua_isboolean(L, -1)) {
		lua_pop(L, 2);
		std::ostringstream stringStream;
		stringStream << "the table variable is not a bool: " << entry;
		throw RuntimeException(stringStream.str());
	}
	bool b = lua_toboolean(L, -1);
	lua_pop(L, 2);
	return b;
}

void LuaConfig::registerFunction(char name[], lua_CFunction luaFunctionl)
{
	lua_pushcfunction(L, luaFunctionl);
	lua_setglobal(L, name);
}

LuaConfig::~LuaConfig()
{
}
