//
// Created by Nils Kübler on 07/07/15.
//

#include "BinaryLabel.h"
#include "mathhelpers.h"

BinaryLabel::BinaryLabel(string name, int size) : Entity(name), size(size)
{
}

void BinaryLabel::enable()
{
    for (int i = 0; i < this->size; ++i) {
        Sprite *s = new Sprite("s");
        s->setParent(this);
        s->enable();
    }
}

void BinaryLabel::disable()
{
    while ((*this)[0]) {
        Sprite *s = (Sprite *) (*this)[0];
        s->setParent(NULL);
        s->disable();
    }
}


void BinaryLabel::update(float deltaTime)
{
    Entity::update(deltaTime);

    if (this->lastNumber != this->number) {
        this->lastNumber = this->number;
        this->updateNumber();
    }

    if(this->lastPosition != this->position || this->lastScale != this->scale || this->lastVisible != visible) {
        this->lastPosition = this->position;
        this->lastScale = this->scale;
        this->lastVisible = this->visible;
        this->updateContent();
    }
}

void BinaryLabel::updateNumber()
{
    int rest = this->number;
    int i = this->size -1;
    while(i >= 0) {
        int currentDigit = rest % 2;
        if(currentDigit > 0) {
            ((Sprite*)(*this)[i])->loadSprite("One.bmp");
        } else {
            ((Sprite*)(*this)[i])->loadSprite("Zero.bmp");
        }
        i--;
        rest = rest >> 1;
    }
}

void BinaryLabel::updateContent()
{
    int numChildren = this->numChildren();

    float width = 0;
    float height = 0;

    for (int i = 0; i < numChildren; ++i) {
        Sprite *s = (Sprite *) (*this)[i];
        s->scale = this->scale;
        width += s->size().x;
        height = std::fmax(height, s->size().y);
    }

    Vector2<float> currentPosition = this->position;
    currentPosition.x -= width / 2;

    for (int i = 0; i < numChildren; ++i) {
        Sprite *s = (Sprite *) (*this)[i];
        currentPosition.x += s->size().x / 2;
        s->position = currentPosition;
        currentPosition.x += s->size().x / 2;
        s->visible = this->visible;
    }
}
