#pragma once
#include "PhysicalSprite.h"
#include "ParticleSystem.h"

class Player;

class Bullet :
	public PhysicalSprite
{
private:
	Player *player;
	Entity *enemies;
	void bounceBackTarget(PhysicalSprite *entity);

public:
	ParticleSystem particles;

	bool outOfRange;
	float strength;
	float lifeTime;
	void update(float deltaTime);
	void enable();
	void disable();

	Bullet();
	virtual ~Bullet();
};

