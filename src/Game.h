#pragma once
#include "Entity.h"

#include <SDL2/SDL.h>

#include "GameScene.h"

class Game :
	public Entity
{
private:
	GameScene gameScene;

public:

	Game();
	~Game();
	virtual void enable();
};

