#pragma once

#include "Vector2.h"
#include <SDL2/SDL.h>
#include "PhysicalSprite.h"
#include "Vector2.h"
#include "Bullets.h"
#include "Cannon.h"
#include "ParticleSystem.h"	

class Player : public PhysicalSprite
{
private:
	Vector2<float> mousePosition;
	Vector2<float> mouseDir;
	float holdMouseSince = 0;

	ParticleSystem boundsParticles;
	float loadShootDuration;

	Entity *enemies;

	float shootingFactor = 0;
	SoundResource chargeSound;

	bool chargeSoundPlayed;

	SoundResource edgeSound;

public:

	Player();
	virtual ~Player();
	void onSdlEvent(SDL_Event *event);
	virtual void update(float deltaTime);
	virtual void enable();
	virtual void disable();
	virtual void edgeHit(Edge edge);
	float getPastTime();
	float getShootingFactor();


};

