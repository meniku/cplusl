#include "Resources.h"
#include <iostream>
using namespace std;

class LoadResourceException : public ResourceException
{
private:
	string error;
public:
	LoadResourceException(string error) :error(error) {
	}
	virtual ~LoadResourceException() throw() {}

	virtual const char* what() const throw()
	{
		return error.c_str();
	}
};

Resources::Resources(SDL_Renderer *renderer) : renderer(renderer)
{
}

Resources::~Resources()
{
	this->destroyAll();
}

void Resources::destroyAll()
{
	cout << "Destroying all resources " << endl;
	for (auto& x : textures) {
		if(x.second.valid) {
			SDL_DestroyTexture(x.second.texture);
		}
	}
	textures.clear();

	cout << "Destroying all musics " << endl;
	for (auto& x : musics) {
		if(x.second.valid) {
			Mix_FreeMusic(x.second.music);
		}
	}
	musics.clear();

	cout << "Destroying all sounds " << endl;
	for (auto& x : sounds) {
		if(x.second.valid) {
			Mix_FreeChunk(x.second.sound);
		}
	}
	sounds.clear();
}

TextureResource Resources::getTexture(string path)
{
	TextureResource texture = textures[path];
	
	if (!texture.valid) {
		cout << "loading texture at " << path << endl;
		SDL_Surface *bitmapSurface = SDL_LoadBMP(path.c_str());
		if(!bitmapSurface) {
			throw LoadResourceException("Could not load texture at " + path);
		}
		SDL_Texture *bitmapTex = SDL_CreateTextureFromSurface(renderer, bitmapSurface);
		SDL_assert(bitmapTex != NULL);
		texture.size.x = bitmapSurface->w;
		texture.size.y = bitmapSurface->h;
		SDL_FreeSurface(bitmapSurface);
		texture.texture = bitmapTex;
		texture.valid = true;
		textures[path] = texture;
	}

	return texture;
}

MusicResource Resources::getMusic(string path)
{
	MusicResource &music = musics[path];
	if (!music.valid) {
		cout << "loading music at " << path << endl;
		music.music  = Mix_LoadMUS(path.c_str());
		if(!music.music) {
			throw LoadResourceException("Could not load music at " + path);
		}
		music.valid = true;
	}
	return music;
}

SoundResource Resources::getSound(string path)
{
	SoundResource &sound = sounds[path];
	if (!sound.valid) {
		cout << "loading sound at " << path << endl;
		sound.sound  = Mix_LoadWAV(path.c_str());
		if(!sound.sound) {
			throw LoadResourceException("Could not load sound at " + path);
		}
		sound.valid = true;
	}
	return sound;
}
