#include "Bullets.h"

Bullets::Bullets() : Entity("Bullets"), pool(NUM_BULLETS)
{
}

Bullet * Bullets::assignBullet()
{
	return pool.alloc();
}

void Bullets::remove(Bullet *bullet)
{
    if(bullet->getParent() == this) {
        bullet->disable();
        bullet->setParent(NULL);
        pool.dealloc(bullet);
    }
}

Bullet * Bullets::spawn()
{
	Bullet *bullet = this->assignBullet();
	if (!bullet) {
		cout << "NO BULLETS LEFT";
		return NULL;
	}
	bullet->outOfRange = false;
	bullet->lifeTime = 99999999;
	bullet->setParent(this);
	bullet->enable();
	return bullet;
}

void Bullets::disable()
{
	int i = 0;
	while (i < numChildren()) {
		Bullet *bullet = (Bullet*)((*this)[i]);
		this->remove(bullet);
	}
}

void Bullets::update(float deltaTime)
{
	int i = 0;
	while (i < numChildren()) {
		Bullet *bullet = (Bullet*)((*this)[i]);
		if (bullet->lifeTime < 0) {
			this->remove(bullet);
		}
		else {
			i++;
		}
	}
}

Bullets::~Bullets()
{
}
