#pragma once

#include "Entity.h"
#include "Player.h"
#include "Bullets.h"
#include "Sprite.h"
#include "Particles.h"
#include "Enemy.h"
#include "LuaConfig.h"
#include "BinaryLabel.h"

class GameScene : public Sprite
{
private:
	bool gotFirstWeapon = false;
	int level = 0;
	int currentScore =0;
	int bestScore = 0;
	MusicResource music;
	SoundResource dieSound;

public:
	float pastTime = 0;
	Player player;
	Bullets bullets;
	Entity enemies;
	Entity cannons;
    Particles particles;
	BinaryLabel currentScoreLabel;
	BinaryLabel bestScoreLabel;
	Sprite bestScoreLabelSprite;
	Sprite introductionSprite;

	GameScene();
	~GameScene();

	virtual void enable();

	virtual void update(float deltaTime);

	virtual string fetchStats();

	void restart();

	void increaseScore();

	void gotWeapon();

	void shotCannon();
};

int spawnEnemy(lua_State *L);

int spawnWeapon(lua_State *L);