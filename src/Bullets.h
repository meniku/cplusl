#pragma once

#include "Entity.h"
#include "Bullet.h"
#include "Vector2.h"
#include <list>
#include "FixedPool.h"

#define NUM_BULLETS 100

class Bullets : public Entity
{
private:
    FixedPool<Bullet> pool;
    
	Bullet *assignBullet();
    
public:
	Bullets();
    
	virtual ~Bullets();

	virtual void update(float deltaTime);

	virtual void disable();

	Bullet * spawn();
    
    void remove(Bullet *bullet);
};

