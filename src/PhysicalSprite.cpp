#include "PhysicalSprite.h"
#include "World.h"
#include "mathhelpers.h"

PhysicalSprite::PhysicalSprite(string name) : Sprite(name), velocity(0, 0), angularVelocity(0), _isEdgeHit(false)
{
}


PhysicalSprite::~PhysicalSprite()
{
}

void PhysicalSprite::enable()
{
	_wasOnScreen = false;
	_isOffScreen = false;
	_isEdgeHit = false;
}

void PhysicalSprite::update(float deltaTime) 
{
	rotation += angularVelocity * deltaTime;
	position += velocity * deltaTime;

	Vector2<float> topLeft = getWorld()->topLeft;
	Vector2<float> bottomRight = getWorld()->bottomRight;

	Vector2<float> halfSize = this->size() / 2;

	_isEdgeHit = false;

	_isOffScreen = position.x < topLeft.x - halfSize.x || position.x > bottomRight.x + halfSize.x || position.y > bottomRight.y + halfSize.y || position.y < topLeft.y - halfSize.y;

	if(!_isOffScreen) {
		_wasOnScreen = true;
	}

	if (velocity.x < 0 && position.x < topLeft.x + halfSize.x) {
		edgeHit(LEFT);
	}
	if (velocity.x > 0 && position.x > bottomRight.x - halfSize.x) {
		edgeHit(RIGHT);
	}
	if (velocity.y < 0 && position.y < topLeft.y + halfSize.y) {
		edgeHit(TOP);
	}
	if (velocity.y > 0 && position.y > bottomRight.y - halfSize.y) {
		edgeHit(BOTTOM);
	}
}

void PhysicalSprite::edgeHit(Edge edge)
{
	_isEdgeHit = true;
	if (stayInBounds) {
		if (edge == TOP || edge == BOTTOM) {
			velocity.y *= -1;
		}
		else {
			velocity.x *= -1;
		}
	}
}

bool PhysicalSprite::collidesWith(const PhysicalSprite & other)
{
	if (!this->collisionEnabled || !other.collisionEnabled) {
		return false;
	}
	Vector2<float> size = this->size();
	Vector2<float> otherSize = other.size();

	return intersectCircle(this->position, size.x / 2, other.position, otherSize.x / 2);
	/*
	SDL_Rect a, b;
	Vector2<float> size = this->size();
	Vector2<float> otherSize = other.size();

	a.x = this->position.x - size.x / 2;
	a.y = this->position.y - size.y / 2;
	a.w = size.x;
	a.h = size.y;

	b.x = other.position.x - otherSize.x / 2;
	b.y = other.position.y - otherSize.y / 2;
	b.w = otherSize.x;
	b.h = otherSize.y;

	return SDL_IntersectRect(&a, &b, result);*/
}