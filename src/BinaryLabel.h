//
// Created by Nils Kübler on 07/07/15.
//

#ifndef CPLUSL_BINARYLABEL_H
#define CPLUSL_BINARYLABEL_H

#include "Entity.h"
#include "Sprite.h"

class BinaryLabel : public Entity
{
private:
    int lastNumber = -1;
    int size = 8;

    Vector2<float> lastPosition;
    Vector2<float> lastScale;
    bool lastVisible;

public:
    BinaryLabel(string name, int size);

    int number = 0;

    virtual void update(float deltaTime) override;

    void updateNumber();

    virtual void enable() override;

    virtual void disable() override;

    void updateContent();

    bool visible = true;
};


#endif //CPLUSL_BINARYLABEL_H

