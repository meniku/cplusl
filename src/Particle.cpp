#include "Particle.h"
#include "ParticleSystem.h"
#include "mathhelpers.h"

Particle::Particle() : Sprite("p")
{
}


Particle::~Particle()
{
}


void Particle::enable()
{

}

void Particle::update(float deltaTime)
{
	livedTime += deltaTime;
	if (livedTime > lifeTime) {
        ((ParticleSystem*)this->getParent())->remove(this);
		return;
	}

	float l = (lifeTime / 2);

	if (livedTime < l) {
		scale.xy(lerp(livedTime / l, startScale, midScale));
	}
	else {
		scale.xy(lerp((livedTime - l) / l, midScale, endScale));
	}

	this->position += this->velocity * deltaTime;
}
