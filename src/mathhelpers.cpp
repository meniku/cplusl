#include "mathhelpers.h"
using namespace std;

float lerp(float t, float a, float b)
{
	return (1 - t)*a + t*b;
}

float randomFloat(float a, float b)
{
	float random = ((float)rand()) / (float)RAND_MAX;
	float diff = b - a;
	float r = random * diff;
	return a + r;
}

bool intersectCircle(const Vector2<float> pos1, const float rad1, const Vector2<float> pos2, const float rad2)
{
	return pos1.Distance(pos2) < rad1 + rad2;
}

