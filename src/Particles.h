//
//  Particles.h
//  CplusL
//
//  Created by Nils Kübler on 26/06/15.
//
//

#pragma once

#include "Entity.h"
#include "Particle.h"
#include "FixedPool.h"

#define NUM_PARTICLES 15000

class Particles : public Entity
{
public:
    FixedPool<Particle> pool;
    
    Particles();
    virtual ~Particles();
    
};