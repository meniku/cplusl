#pragma once

#include <cstdlib>
#include <math.h>       /* sqrt */

using namespace std;

template <typename T> class Vector2
{
public:
	T x;
	T y;

	Vector2(T x, T y) : x(x), y(y){	}
	Vector2(void) : x(0), y(0) { }
	Vector2(const Vector2 * v) : x(v->x), y(v->y) { };
	Vector2(T angle){
		x = cos(angle);
		y = sin(angle);
	}
  
	void Set(T xValue, T yValue) { x = xValue; y = yValue; }
	void xy(T xy) { x = xy; y = xy; }
	void xy(T x, T y) { this->x = x; this->y = y; }

	T Length() const { return sqrt(x * x + y * y); };
	T LengthSquared() const { return x * x + y * y; };
	T Distance(const Vector2<T> & v) const { return sqrt(((x - v.x) * (x - v.x)) + ((y - v.y) * (y - v.y))); };
	T DistanceSquared(const Vector2 & v)  const { return ((x - v.x) * (x - v.x)) + ((y - v.y) * (y - v.y)); };
	T Dot(const Vector2 & v)  const { return x * v.x + y * v.y; };
	T Cross(const Vector2 & v)  const { return x * v.y - y * v.x; };

	T Angle() {
		return atan2(y, x);
	}

	/*template <typename T>
	void Truncate(T upperBound)
	{
		T sqrLen = SqrLength();
		if (sqrLen > upperBound * upperBound)
		{
			T mult = upperBound / sqrt(sqrLen);
			x *= mult; y *= mult;
		}
	}*/

	Vector2 Normalized() const {
		if (LengthSquared() != 0)
		{
			T length = Length();
			return Vector2(x / length, y / length);
		}
		return Vector2(0, 0);
	}

	//VECTOR2 TO VECTOR2 OPERATIONS

	//ASSINGMENT AND EQUALITY OPERATIONS
	inline Vector2 & operator = (const Vector2 & v) { x = v.x; y = v.y; return *this; }
	inline Vector2 & operator = (const T & f) { x = f; y = f; return *this; }
	//inline Vector2 & operator - (void) { x = -x; y = -y; return *this; }
	inline bool operator == (const Vector2 & v) const { return (x == v.x) && (y == v.y); }
	inline bool operator != (const Vector2 & v) const { return (x != v.x) || (y != v.y); }

	//VECTOR2 TO VECTOR2 OPERATIONS
	inline const Vector2 operator + (const Vector2 & v) const { return Vector2(x + v.x, y + v.y); }
	inline const Vector2 operator - (const Vector2 & v) const { return Vector2(x - v.x, y - v.y); }
	inline const Vector2 operator * (const Vector2 & v) const { return Vector2(x * v.x, y * v.y); }
	inline const Vector2 operator / (const Vector2 & v) const { return Vector2(x / v.x, y / v.y); }

	//VECTOR2 TO THIS OPERATIONS
	inline Vector2 & operator += (const Vector2 & v) { x += v.x; y += v.y; return *this; }
	inline Vector2 & operator -= (const Vector2 & v) { x -= v.x; y -= v.y; return *this; }
	inline Vector2 & operator *= (const Vector2 & v) { x *= v.x; y *= v.y; return *this; }
	inline Vector2 & operator /= (const Vector2 & v) { x /= v.x; y /= v.y; return *this; }

	//SCALER TO VECTOR2 OPERATIONS
	inline const Vector2 operator + (const T v) const { return Vector2(x + v, y + v); }
	inline const Vector2 operator - (const T v) const { return Vector2(x - v, y - v); }
	inline const Vector2 operator * (const T v) const { return Vector2(x * v, y * v); }
	inline const Vector2 operator / (const T v) const { return Vector2(x / v, y / v); }

	//SCALER TO THIS OPERATIONS
	inline Vector2 & operator += (T v) { x += v; y += v; return *this; }
	inline Vector2 & operator -= (T v) { x -= v; y -= v; return *this; }
	inline Vector2 & operator *= (T v) { x *= v; y *= v; return *this; }
	inline Vector2 & operator /= (T v) { x /= v; y /= v; return *this; }

	//~Vector2();
};

