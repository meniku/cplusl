#pragma once

#include "Entity.h"
#include "Sprite.h"
#include "Particle.h"
#include <list>
#include "Particles.h"

class ParticleSystem : public Entity
{
    friend class Particle;
private:
    Particles *particles;
	float blockedTime = 0;
	bool running = false;
	float runningTime = 0;
    
    void remove(Particle *particle);
public:
	bool loop = false;
	float duration = 1;
	float maxParticles = 10;
	float lifeTime = 1;
	float startScale = 0;
	float midScale = 0.5;
	float endScale = 0;
	float emitDelay = 0;
	float emitMinAngle = 45;
	float emitMaxAngle = 90;
	float startVelocity = 0.1f;
	string sprite;
	bool useOldestWhenNoMoreParticles = false;

//	list<Particle *> particles;

	void loadFromConfig(char c_key[]);
	ParticleSystem();
	~ParticleSystem();

	void start();
	void stop();
	bool emit();
	void update(float deltaTime);
	void enable();
	void disable();

	Particle * getFreeParticle();
};

