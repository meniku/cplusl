#pragma once
#include "PhysicalSprite.h"
#include "ParticleSystem.h"	
#include "Bullet.h"

class Enemy :
	public PhysicalSprite
{
	
private:
	float particlesMinAngle;
	float particlesMaxAngle;
	SoundResource sound;
	SoundResource killSound;
public:
	ParticleSystem particleSystem;
	float isDeadSince = -1;
	bool isDead();
	Enemy();
	virtual ~Enemy();
	virtual void enable();
	virtual void disable();
	virtual void update(float deltaTime);
	virtual void hit(Vector2<float> dir, float strength);
};

