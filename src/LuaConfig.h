#pragma once

extern "C" {
#include "lua5.3/lua.h"
#include "lua5.3/lualib.h"
#include "lua5.3/lauxlib.h"
}

#include <exception>
#include <string>

class LuaConfigException : public std::exception {
};

class LuaConfig
{
public:
	LuaConfig();
	~LuaConfig();

	bool getBool(char section[], char name[]);
	int getInt(char name[]);
	int getInt(char section[],  char name[]);
	float getFloat(char name[]);
	float getFloat(const char section[], const char name[]);
	std::string getString(char name[]);
	std::string getString(char section[], char entry[]);


	void call(char table[], char function[], float arg1);
	void call(char table[], char function[], float arg1, float arg2);
	void call(char table[], char function[], float arg1, int arg2);
	float callFloat(char table[], char function[], float arg1);
	float callFloat(char table[], char function[], float arg1, float arg2);

	void registerFunction(char name[], lua_CFunction luaFunctionl);

	lua_State *L;

};

