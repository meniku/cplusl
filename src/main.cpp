#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include "LuaConfig.h"
#include "Player.h"
#include "World.h"
#include "Bullets.h"
#include <iostream>
#include "Game.h"

#ifdef __APPLE__
#include "unistd.h"
#else
#include <Windows.h>
#endif

using namespace std;

int main(int argc, char* argv[])
{
    SDL_Window *win = NULL;
    SDL_Renderer *renderer = NULL;

    bool waitForKeyPress = false;

    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        return 1;
    }

	std::cout << "Setting Working directory to ... " << SDL_GetBasePath() << std::endl;
#ifdef __APPLE__
    chdir(SDL_GetBasePath());
#else
	SetCurrentDirectory(SDL_GetBasePath());
#endif
	
    try {
        LuaConfig luaConfig;
        win = SDL_CreateWindow(luaConfig.getString("engine", "window_title").c_str(), 110, 110,
                               luaConfig.getInt("engine", "window_width"), luaConfig.getInt("engine", "window_height"), 0);
        bool vsync = luaConfig.getBool("engine", "vsync");

        int flags = SDL_RENDERER_ACCELERATED;
        if (vsync) flags |= SDL_RENDERER_PRESENTVSYNC;
        renderer = SDL_CreateRenderer(win, -1, flags);

        if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 4, 4096) == -1) {
            cerr << "could not initialize audio system" << endl;
        }

        bool running = true;
        SDL_Event event;

        const float fps = luaConfig.getFloat("engine", "fps");
        const float maxFps = luaConfig.getFloat("engine", "max_fps");
        const float defaultFrameDuration = 1 / fps;
        const float maxFpsFrameDuration = 1 / maxFps;

        // TODO: FMod, SDLMixer or OpenAL

        Resources resources(renderer);

        World world(&resources, renderer, &luaConfig);
		world.setScreenRect(luaConfig.getInt("engine", "window_width"), luaConfig.getInt("engine", "window_height"));

        Game game;
        game.setParent(&world);

        world.enableEntities(&world);

        Uint64 freq = SDL_GetPerformanceFrequency();

        int numFrames = 0;
        Uint64 now = SDL_GetPerformanceCounter();
        Uint64 previous;
        float fpsCounterTime = 0;
        float lastFrameDuration = 0;
        bool paused = false;

        while (running)
        {
            previous = now;

            // wait if we're over max fps
            now = SDL_GetPerformanceCounter();
            if (maxFps > -1) {
                float frameDuration = ((float)(((now - previous) * 1000000) / freq) / 1000000);
                if (frameDuration < maxFpsFrameDuration) {
                    SDL_Delay((int)((maxFpsFrameDuration - frameDuration) * 1000));
                    now = SDL_GetPerformanceCounter();
                }
            }

            // measure frame duration
            lastFrameDuration = ((float)(((now - previous) * 1000000) / freq) / 1000000);
            numFrames++;
            fpsCounterTime += lastFrameDuration;
            if (fpsCounterTime > 1.0) {
                if (!paused) {
                    cout << "FPS: " << numFrames << " " << world.fetchStatsOnEntities(&world) << endl;
                }
                numFrames = 0;
                fpsCounterTime = 0;
            }

            // Handle Input
            while (SDL_PollEvent(&event) != 0)
            {
                switch (event.type) {
                    case SDL_QUIT:
                        running = false;
                        break;

                    case SDL_KEYDOWN:
                        if (event.key.keysym.sym == SDLK_ESCAPE)
                            running = false;
                        else if (event.key.keysym.sym == SDLK_p)
                            paused = !paused;
                        break;

                    default:
                        world.notifyEntities(&event);
                }
            }

            // Update State
            if (lastFrameDuration > 0 && !paused) {
                world.updateEntities(&world, lastFrameDuration < defaultFrameDuration ? lastFrameDuration : defaultFrameDuration);
            }

            // Remove Deleted Entities
            world.performCleanup();

            // Render
            SDL_RenderClear(renderer);
            world.renderEntities(&world, renderer);
            SDL_RenderPresent(renderer);
        }
    }
    catch (LuaConfigException &e) {
        cerr << "lua exception: " << e.what();
#ifndef __APPLE__
		waitForKeyPress = true;
#endif
    }
    catch (ResourceException &e) {
        cerr << "resource exception: " << e.what();
#ifndef __APPLE__
		waitForKeyPress = true;
#endif
    }

    if (renderer) {
        SDL_DestroyRenderer(renderer);
    }
    if (win) {
        SDL_DestroyWindow(win);
    }
    SDL_Quit();

    if (waitForKeyPress) {
        cin.ignore();
    }

    return 0;
}
