#include "Player.h"

#include "World.h"
#include "Bullets.h"
#include "GameScene.h"

#include <iostream>
#include <algorithm>  

using namespace std;

Player::Player() : PhysicalSprite("Player")
{
	boundsParticles.setParent(this);
}


Player::~Player()
{
}

void Player::enable()
{
	this->stayInBounds = true;
	this->chargeSound = this->getWorld()->resources->getSound("charge.wav");
	this->edgeSound = this->getWorld()->resources->getSound("edge_hit.wav");

	boundsParticles.loadFromConfig("particles_bounds");

	loadSprite("Player.bmp");

	getWorld()->addSdlEventListener(SDL_MOUSEBUTTONDOWN, this);
	getWorld()->addSdlEventListener(SDL_MOUSEBUTTONUP, this);
	getWorld()->addSdlEventListener(SDL_MOUSEMOTION, this);

	LuaConfig *luaConfig = this->getWorld()->luaConfig;
	this->loadShootDuration = luaConfig->getFloat("player" , "load_shoot_duration");

	enemies = this->findParentWithName("GameScene")->getChildWithName("Enemies");
}

void Player::disable()
{
	getWorld()->removeSdlEventListener(SDL_MOUSEBUTTONDOWN, this);
	getWorld()->removeSdlEventListener(SDL_MOUSEBUTTONUP, this);
	getWorld()->removeSdlEventListener(SDL_MOUSEMOTION, this);

	Cannon *cannon;
	while ((cannon = (Cannon*)this->getChildWithName("Cannon"))) {
		cannon->disable();
        cannon->markForDeletion();
		cannon->setParent(NULL);
	}

	this->position.Set(0, 0);
	this->velocity.Set(0, 0);
	this->holdMouseSince = 0;
}

void Player::onSdlEvent(SDL_Event *sdlEvent)
{
	Vector2<int> screenPosition;
	SDL_GetMouseState(&screenPosition.x, &screenPosition.y);
	mousePosition = getWorld()->screenPointToWorldPoint(screenPosition);
	mouseDir = (mousePosition - this->position).Normalized();

	switch (sdlEvent->type) {
	case SDL_MOUSEBUTTONDOWN:
		holdMouseSince = this->getPastTime();
		break;
	case SDL_MOUSEMOTION:

		break;
	case SDL_MOUSEBUTTONUP:
		holdMouseSince = 0;
		
		break;
	}
}

void Player::update(float deltaTime)
{
	World *world = getWorld();
	LuaConfig *luaConfig = world->luaConfig;

	PhysicalSprite::update(deltaTime);

	mouseDir = (mousePosition - this->position).Normalized();
	this->rotation = mouseDir.Angle();

	scale.x = luaConfig->callFloat("player", "scale_x", getShootingFactor(), deltaTime);
	scale.y = luaConfig->callFloat("player", "scale_y", getShootingFactor(), deltaTime);

	if(this->getShootingFactor() > 0.2 && !chargeSoundPlayed) {
		chargeSoundPlayed = true;
		Mix_PlayChannel(1, chargeSound.sound, 0);
	} else if(this->getShootingFactor() < 0.2) {
		chargeSoundPlayed = false;
	}

	for (int i = 0; i < this->enemies->numChildren(); ) {
		Enemy *enemy = (Enemy *)(*this->enemies)[i];
		if (this->collidesWith(*enemy)) {
			Vector2<float> dir = enemy->position - this->position;

			boundsParticles.emitMinAngle = -180.f * DEG_TO_RAD;
			boundsParticles.emitMaxAngle = 180.f * DEG_TO_RAD;
			boundsParticles.position = position;
			boundsParticles.start();

			enemy->hit(dir, 1000000);

			Cannon *cannon = (Cannon *) this->getChildWithName("Cannon");
			if (cannon != NULL) {
				cannon->lostFromPlayer(dir);
			}
			else {
				((GameScene*)this->getParent())->restart();
				return;
			}
		}
		else {
			i++;
		}
	}
}

void Player::edgeHit(Edge edge)
{
	PhysicalSprite::edgeHit(edge);
	World *world = getWorld();
	Vector2<float> topLeft = world->topLeft;
	Vector2<float> bottomRight = world->bottomRight;

	if (edge == LEFT) {
		boundsParticles.emitMinAngle = -90.f * DEG_TO_RAD;
		boundsParticles.emitMaxAngle = 90.f * DEG_TO_RAD;
		boundsParticles.position.xy(topLeft.x, position.y);
		boundsParticles.start();
	}
	if (edge == RIGHT) {
		boundsParticles.emitMinAngle = 90.f * DEG_TO_RAD;
		boundsParticles.emitMaxAngle = 270.f * DEG_TO_RAD;
		boundsParticles.position.xy(bottomRight.x, position.y);
		boundsParticles.start();
	}
	if (edge == TOP) {
		boundsParticles.emitMinAngle = 0.f * DEG_TO_RAD;
		boundsParticles.emitMaxAngle = 180.f * DEG_TO_RAD;
		boundsParticles.position.xy(position.x, topLeft.y);
		boundsParticles.start();
	}
	if (edge == BOTTOM) {
		boundsParticles.emitMinAngle = 180.f * DEG_TO_RAD;
		boundsParticles.emitMaxAngle = 360.f * DEG_TO_RAD;
		boundsParticles.position.xy(position.x, bottomRight.y);
		boundsParticles.start();
	}

	Mix_PlayChannel(2, this->edgeSound.sound, 0);
}

float Player::getPastTime() 
{
	GameScene *gameScene = (GameScene *)findParentWithName("GameScene");
	return gameScene->pastTime;
}


float Player::getShootingFactor()
{
	return this->holdMouseSince == 0 ? 0 : min<float>(
		1, (this->getPastTime() - this->holdMouseSince) / this->loadShootDuration
	);
}
