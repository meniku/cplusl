#pragma once

#include "Vector2.h"
#include <SDL2/SDL.h>
#include <string>
#include <vector>
#include <iostream>
#include "mathhelpers.h"

class World;

using namespace std;

class Entity
{
private: 
	Entity *parent = NULL;
	vector<Entity *> children;
	World *world = NULL; 

public:
	bool renderBeforeChildren = true;
	string name;
	Vector2<float> position;
	Vector2<float> scale;
	float rotation;

	Entity(std::string name);
	virtual ~Entity();
    
    void markForDeletion();

	virtual void update(float deltaTime);
	virtual void render(SDL_Renderer *renderer);
	virtual void onSdlEvent(SDL_Event *event);

	Entity *findParentWithName(string name);
	Entity *getChildWithName(string name);
 
	World *getWorld();

	void setParent(Entity *parent);
	Entity * getParent();

	virtual void enable();
	virtual void disable();

	virtual string fetchStats();
    
	int numChildren();
	Entity * operator []  (const int i) const;
	int getChildIndex(Entity *child);
};