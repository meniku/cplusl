#pragma once

#include "Vector2.h"
#include <SDL2/SDL.h>
#include "Entity.h"
#include <string>
#include "Resources.h"


using namespace std;

class Sprite : public Entity
{
public:
	TextureResource texture;
	bool visible = true;
	Sprite(string name);
	virtual ~Sprite();
	virtual void render(SDL_Renderer *renderer);
	void loadSprite(string path);

	Vector2<float> spriteSize;

	virtual Vector2<float> size() const;
};

