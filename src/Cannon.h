#pragma once

#include "Vector2.h"
#include <SDL2/SDL.h>
#include "PhysicalSprite.h"
#include "Vector2.h"
#include "Bullets.h"

class GameScene;

class Cannon : public PhysicalSprite
{
private:
	Bullet * shootingBullet;
	float lastShootingFactor;
	bool lost;
	float angleBetweenWeapons;
	int maxWeapons;
	SoundResource sound;

public:
	Cannon();
	virtual ~Cannon();
	void onSdlEvent(SDL_Event *event);
	virtual void update(float deltaTime);
	virtual void enable();
	virtual void disable();
	float getPastTime();
	void lostFromPlayer(Vector2<float> direction);

	void updateBulletPosition(float shootingFactor, float current) const;

	GameScene *getGameScene();

};

