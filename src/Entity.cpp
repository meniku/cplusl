#include "Entity.h"
#include "World.h"

Entity::Entity(string name) : name(name), rotation(0), position(0, 0), scale(1,1)
{
	
}


Entity::~Entity()
{
}

Entity *Entity::findParentWithName(string name)
{
	if (this->name == name) {
		return this;
	}
	else if (!this->parent) {
		return NULL;
	} else {
		return this->parent->findParentWithName(name);
	}
}

Entity *Entity::getChildWithName(string name)
{
	vector<Entity *>::iterator it;
	for (it = this->children.begin(); it != this->children.end(); ++it) {
		if ((*it)->name == name) {
			return *it;
		}
	}
	return NULL;
}

void Entity::setParent(Entity *parent)
{
	if (this->parent != NULL) {
		int indexOfThis = this->parent->getChildIndex(this);
		this->parent->children.erase(this->parent->children.begin() + indexOfThis);
	}
	this->world = NULL;
	this->parent = parent;
	if (parent != NULL) {
		parent->children.push_back(this);
	}
}

void Entity::update(float deltaTime)
{

}

void Entity::render(SDL_Renderer *renderer)
{

}

void Entity::onSdlEvent(SDL_Event *event)
{

}

int Entity::numChildren()
{
    return children.empty() ? 0 : children.size();
}

Entity * Entity::operator []  (const int i) const
{
	if (this->children.size() > 0) {
		return this->children.at(i);
	}
	else {
		return NULL;
	}
}

int Entity::getChildIndex(Entity * child)
{
	for (int i = 0; i < this->numChildren(); i++)
	{
		if ((*this)[i] == child) {
			return i;
		}
	}
	return -1;
}

World *Entity::getWorld()
{
	if (this->world != NULL) {
		return this->world;
	}

	if (this->parent != NULL) {
		this->world =  this->parent->getWorld();
		return this->world;
	}
	else if(this->name == "World") {
		this->world = (World *) this;
		return this->world;
	}
	else {
		return NULL;
	}
}

Entity * Entity::getParent()
{
	return parent;
}

void Entity::enable()
{

}

void Entity::disable()
{

}

string Entity::fetchStats()
{
	return "";
}

void Entity::markForDeletion()
{
    World *world = getWorld();
    if(world != NULL) {
        world->markForDeletion(this);
    } else {
        cerr << "ERROR: cannot mark " << this->name << " for deletion, as it's not attached to the world" << endl;
    }

}