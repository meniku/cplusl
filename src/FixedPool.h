//
//  EntityPool.h
//  CplusL
//
//  Created by Nils Kübler on 26/06/15.
//
//

#pragma once

#include <iostream>
#include <unordered_map>

using namespace std;

template <typename T> class FixedPool
{
private:
    int numItems;
    int currentIndex;
    T* items;
    bool* inuse;
    std::unordered_map<T*, int> indexMap;
    
public:
    
    FixedPool(int size) :
        numItems(size),
        currentIndex(0)
    {
        items = new T[size];
        inuse = new bool[size];
        for(int i = 0; i < size; i++) {
            inuse[i] = false;
        }
    }
    
    ~FixedPool()
    {
        delete[] items;
        delete[] inuse;
    }
    
    T* alloc()
    {
        for(int i = 0; i < numItems; i++) {
            int index = (currentIndex + i) % numItems;
            if(!inuse[index]) {
                currentIndex = index;
                T *item = &items[index];
                indexMap[item] = index;
                inuse[index] = true;
                return item;
            }
        }
        std::cerr << "WARNING: Pool with size " << numItems << " too small" << endl;
        return NULL;
    }
    
    void dealloc(T* item)
    {
        if(indexMap.count(item)) {
            inuse[indexMap[item]] = false;
        }
    }
};