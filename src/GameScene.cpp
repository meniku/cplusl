#include "GameScene.h"
#include "World.h"
#include "Cannon.h"
#include "LuaConfig.h"
#include <SDL2/SDL_mixer.h>

#include <sstream>

GameScene::GameScene() : Sprite("GameScene"), enemies("Enemies"), cannons("Cannons"),
                         currentScoreLabel("currentScoreLabel", 8),
                         bestScoreLabel("bestScoreLabel", 8),
                         bestScoreLabelSprite(""),
                         introductionSprite("")
{
    player.setParent(this);
    enemies.setParent(this);
    bullets.setParent(this);
    cannons.setParent(this);
    particles.setParent(this);
    currentScoreLabel.setParent(this);
    bestScoreLabel.setParent(this);
    bestScoreLabelSprite.setParent(this);
    introductionSprite.setParent(this);
    this->pastTime = 0;
}

GameScene::~GameScene()
{
}

void GameScene::enable()
{
    this->music = getWorld()->resources->getMusic("music.wav");
    this->dieSound = getWorld()->resources->getSound("die.wav");

    this->loadSprite("Arena.bmp");
    this->scale.x = getWorld()->getScreenWidth() / this->texture.size.x;
    this->scale.y = getWorld()->getScreenHeight() / this->texture.size.y;

    lua_State *L = getWorld()->luaConfig->L;
    lua_pushlightuserdata(L, this);
    lua_setglobal(L, "luaGameScenePointer");

    this->getWorld()->luaConfig->registerFunction("spawnEnemy", spawnEnemy);
    this->getWorld()->luaConfig->registerFunction("spawnWeapon", spawnWeapon);

    this->currentScoreLabel.position = Vector2<float>(0, (float) (this->getWorld()->bottomRight.y - 0.05));
    this->currentScoreLabel.scale = Vector2<float>(4, 4);
    this->currentScoreLabel.visible = false;

    this->bestScoreLabel.position = this->getWorld()->bottomRight;
    this->bestScoreLabel.position.x -= 0.14;
    this->bestScoreLabel.position.y -= 0.05;
    this->bestScoreLabel.scale = Vector2<float>(2, 2);
    this->bestScoreLabel.visible = false;

    this->bestScoreLabelSprite.loadSprite("Score_Best.bmp");
    this->bestScoreLabelSprite.position.x = bestScoreLabel.position.x;
    this->bestScoreLabelSprite.position.y = bestScoreLabel.position.y + 0.018;
    this->bestScoreLabelSprite.visible = false;

    this->introductionSprite.loadSprite("Introduction.bmp");
    this->introductionSprite.scale.xy(3);
    this->introductionSprite.position.y = -0.1;
    this->introductionSprite.visible = false;
}

void GameScene::update(float deltaTime)
{
    this->pastTime += deltaTime;

    if (enemies.numChildren() == 0) {
        this->getWorld()->luaConfig->call("game_scene", "spawn_enemies", this->pastTime, level);
        level++;
    }
    else {
    }
}

void GameScene::restart()
{
    cout << "Game Over Man! Score: " << currentScore << endl;

    Mix_PlayChannel(3, dieSound.sound, 0);

    Enemy *enemy;
    while ((enemy = (Enemy *) this->enemies[0])) {
        enemy->disable();
        enemy->markForDeletion();
        enemy->setParent(NULL);
    }

    Cannon *cannon;
    while ((cannon = (Cannon *) this->cannons[0])) {
        cannon->disable();
        cannon->markForDeletion();
        cannon->setParent(NULL);
    }

    this->player.disable();
    this->player.enable();
    this->level = 0;

    if (this->currentScore > this->bestScore) {
        this->bestScore = this->currentScore;
        this->bestScoreLabel.number = this->currentScore;
        this->bestScoreLabel.visible = true;
        this->bestScoreLabelSprite.visible = true;
    }

    this->currentScore = 0;
    this->currentScoreLabel.visible = false;

    this->gotFirstWeapon = false;

    Mix_FadeOutMusic(500);
}

string GameScene::fetchStats()
{
    std::ostringstream stringStream;
    stringStream << "Enemies:" << enemies.numChildren() << " Cannons:" << cannons.numChildren();
    return stringStream.str();
}

void GameScene::increaseScore()
{
    this->currentScoreLabel.visible = true;
    this->currentScore++;
    this->currentScoreLabel.number = this->currentScore;
}

void GameScene::gotWeapon()
{
    if(!this->gotFirstWeapon) {
        this->gotFirstWeapon = true;
        Mix_PlayMusic(this->music.music, -1);
        this->introductionSprite.visible = true;
    }
}

void GameScene::shotCannon()
{
    this->introductionSprite.visible = false;
}


int spawnEnemy(lua_State *L)
{
    lua_getglobal(L, "luaGameScenePointer");
    GameScene *gameScene = (GameScene *) lua_touserdata(L, -1);
    lua_pop(L, 1);

    Enemy *enemy = new Enemy();
    enemy->position.x = luaL_checknumber(L, 1);
    enemy->position.y = luaL_checknumber(L, 2);
    enemy->scale.xy(luaL_checknumber(L, 3));
    enemy->velocity = Vector2<float>(luaL_checknumber(L, 4), luaL_checknumber(L, 5));
    enemy->angularVelocity = luaL_checknumber(L, 6);
    enemy->setParent(&gameScene->enemies);
    enemy->enable();

    cout << "spawned Enemy at " << enemy->position.x << " " << enemy->position.y << " velocity " << enemy->velocity.x <<
    " " << enemy->velocity.y << endl;
    return 0;  /* number of results */
}

int spawnWeapon(lua_State *L)
{
    lua_getglobal(L, "luaGameScenePointer");
    GameScene *gameScene = (GameScene *) lua_touserdata(L, -1);
    lua_pop(L, 1);

    Cannon *cannon = new Cannon();
    cannon->position.x = luaL_checknumber(L, 1);
    cannon->position.y = luaL_checknumber(L, 2);
    cannon->scale.xy(4);
    cannon->angularVelocity = 1;
    cannon->velocity = Vector2<float>(luaL_checknumber(L, 3), luaL_checknumber(L, 4));
    cannon->setParent(&gameScene->cannons);
    cannon->enable();

    cout << "spawned Cannon at " << cannon->position.x << " " << cannon->position.y << " velocity " <<
    cannon->velocity.x << " " << cannon->velocity.y << endl;
    return 0;  /* number of results */
}
