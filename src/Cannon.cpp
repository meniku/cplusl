#include "Cannon.h"

#include "World.h"
#include "Bullets.h"
#include "GameScene.h"

#include <iostream>
#include <algorithm>  

using namespace std;

Cannon::Cannon() : PhysicalSprite("Cannon"), shootingBullet(NULL)
{
}


Cannon::~Cannon()
{
}

void Cannon::enable()
{
	this->sound = getWorld()->resources->getSound("shoot.wav");
	stayInBounds = false;
	loadSprite("Cannon.bmp");
	scale.x = 4;
	scale.y = 4;

	angleBetweenWeapons = this->getWorld()->luaConfig->getFloat("cannon", "angle_between_weapons") * DEG_TO_RAD;
	maxWeapons = this->getWorld()->luaConfig->getInt("cannon" , "max_weapons");

	Player *player = (Player *)findParentWithName("Player");
	if (player != NULL) {
		getWorld()->addSdlEventListener(SDL_MOUSEBUTTONDOWN, this);
		getWorld()->addSdlEventListener(SDL_MOUSEBUTTONUP, this);
	}
}

void Cannon::disable()
{
	getWorld()->removeSdlEventListener(SDL_MOUSEBUTTONDOWN, this);
	getWorld()->removeSdlEventListener(SDL_MOUSEBUTTONUP, this);
	if (this->shootingBullet != NULL) {
		Bullets *bullets = (Bullets *) this->getParent()->getParent()->getChildWithName("Bullets");
		bullets->remove(shootingBullet);
		shootingBullet = NULL;
	}
}

void Cannon::onSdlEvent(SDL_Event *sdlEvent)
{
	Vector2<int> screenPosition;
	SDL_GetMouseState(&screenPosition.x, &screenPosition.y);
	Player *player = (Player *)findParentWithName("Player");
	LuaConfig *luaConfig = this->getWorld()->luaConfig;
	Bullets *bullets = (Bullets *) this->getParent()->getParent()->getChildWithName("Bullets");
	Bullet * bullet;
	switch (sdlEvent->type) {
	case SDL_MOUSEBUTTONDOWN:
		bullet = bullets->spawn();
		if (bullet) {
			this->shootingBullet = bullet;
            updateBulletPosition(0, 0);
		}
			break;
	case SDL_MOUSEBUTTONUP:
		bullet = this->shootingBullet;
		if (bullet) {
            float shootingFactor = this->lastShootingFactor;
			bullet->lifeTime = luaConfig->callFloat("cannon", "bullet_life_time", shootingFactor);
			Vector2<float> direction = Vector2<float>(this->rotation);
			float speed = luaConfig->callFloat("cannon", "bullet_shoot_speed", shootingFactor);

			shootingBullet->strength = shootingFactor;
			shootingBullet->velocity = direction * speed;

			this->shootingBullet = NULL;
			float rebound = luaConfig->callFloat("cannon", "rebound", shootingFactor);
			player->velocity -= (bullet->velocity.Normalized() * rebound);

            this->getGameScene()->shotCannon();

			Mix_PlayChannel(1, this->sound.sound, 0);
		}
		break;
	}
}

void Cannon::update(float deltaTime)
{
	Player *player = (Player *) findParentWithName("Player");

	if (player != NULL) { // attached to player

		float numberOfCannons = (float) player->numChildren();
		float thisIndex = (float) player->getChildIndex(this);
		float angle = - (numberOfCannons * angleBetweenWeapons) /2 + thisIndex * angleBetweenWeapons;

		this->position = player->position + Vector2<float>(player->rotation + angle) * 0.03f;
		Vector2<float> dir = (this->position - player->position);
		this->rotation = dir.Angle();

		// TODO: Move to LUA
		float shootingFactor = player->getShootingFactor();
		static float current = 0;
		float shootingSpeed = 1.0f + shootingFactor * 16;
		current += shootingSpeed * deltaTime;
		current = fmod(current, M_PI * 2.f);

        updateBulletPosition(shootingFactor, current);

        float shootingSize = 1 + shootingFactor * 4;
		scale.x = 4 + shootingSize * sin(current * 4) * 0.25f;
		scale.y = 4 + shootingSize * sin((1 + current * 4)) * 0.25f;
        this->lastShootingFactor = shootingFactor;
	}
	else { // not attached to player

		player = (Player *)findParentWithName("GameScene")->getChildWithName("Player");

		PhysicalSprite::update(deltaTime);

		if (this->isEdgeHit() && this->isOffScreen()) {
			if (player->getChildWithName("Cannon") == NULL && this->getParent()->numChildren() == 1) {
				// game over when no more weapon on screen
				getGameScene()->restart();
			}
			else {
				this->disable();
                this->markForDeletion();
				this->setParent(NULL);
			}
			return;
		}

		if (this->collidesWith(*player)) {
			if (!lost) {
				this->disable();
				if (player->numChildren() <= maxWeapons) {
					this->setParent(player);
					this->enable();
                    getGameScene()->gotWeapon();
				}
				else {
                    this->markForDeletion();
					this->setParent(NULL);
				}
			}
		}
		else {
			if (lost) {
				lost = false;
			}
		}
	}
}

GameScene *Cannon::getGameScene()
{
    return ((GameScene*) findParentWithName("GameScene"));
}

void Cannon::updateBulletPosition(float shootingFactor, float current) const
{
    if (shootingBullet) {
        Bullet * bullet = shootingBullet;
        bullet->rotation = rotation;
        bullet->position = position + Vector2<float>(bullet->rotation) * 0.018f;
        bullet->scale.xy(2 + shootingFactor * 2 + sin(current * 12) * 0.5f);
    }
}

float Cannon::getPastTime()
{
	return getGameScene()->pastTime;
}

void Cannon::lostFromPlayer(Vector2<float> direction)
{
	this->lost = true;
	this->velocity = direction;
	this->angularVelocity = direction.Angle();
	this->disable();
	setParent(findParentWithName("GameScene")->getChildWithName("Cannons"));
	this->enable();
}
