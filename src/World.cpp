#include "World.h"


World::World(Resources *resources, SDL_Renderer *renderer,LuaConfig *luaConfig)
	: Entity("World"), resources(resources), renderer(renderer),  luaConfig(luaConfig)
{
}


World::~World()
{
    this->performCleanup();
}

void World::update(float deltaTime)
{
	this->timeSinceStart += deltaTime;
}

void World::enable()
{
}

void World::setScreenRect(int width, int height) 
{
	screenRect.x = 0;
	screenRect.y = 0;
	screenRect.w = width;
	screenRect.h = height;
	topLeft = screenPointToWorldPoint(new Vector2<int>(screenRect.x, screenRect.y));
	bottomRight = screenPointToWorldPoint(new Vector2<int>(screenRect.x + screenRect.w, screenRect.y + screenRect.h));
}

Vector2<float> World::screenPointToWorldPoint(Vector2<int> screenPoint)
{
	return Vector2<float>(
		-0.5f + (float)screenPoint.x / (float)screenRect.w,
		(float)(screenPoint.y - screenRect.h / 2) / (float)screenRect.w // we want to use Width, that's true!
	);
}

Vector2<int> World::worldPointToScreenPoint(Vector2<float> worldPoint)
{
	//float aspect = (screenRect.h / screenRect.w);

	return Vector2<int>(
		(int)round((float)screenRect.w / 2 + worldPoint.x * (float)screenRect.w),
		(int)round((float)screenRect.h / 2 + worldPoint.y * (float)screenRect.w) // we want to use Width, that's true!
	);
}

float World::pixelToWorldUnits(int pixels)
{
	return (float)pixels / (float)screenRect.w;
}

int World::worldUnitsToPixels(float units)
{
	return (int)(units * (float)screenRect.w);
}

void World::updateEntities(Entity *entity, float deltaTime)
{
	entity->update(deltaTime);

	for (int i = 0; i < entity->numChildren(); i++) {
        Entity *child = (*entity)[i];
        updateEntities(child, deltaTime);
	}
}

void World::enableEntities(Entity *entity)
{
	entity->enable();

	for (int i = 0; i < entity->numChildren(); i++) {
		enableEntities((*entity)[i]);
	}
}

void World::disableEntities(Entity *entity)
{
	entity->disable();

	for (int i = 0; i < entity->numChildren(); i++) {
		disableEntities((*entity)[i]);
	}
}

string World::fetchStatsOnEntities(Entity *entity)
{
	string allStats = entity->fetchStats();

	for (int i = 0; i < entity->numChildren(); i++) {
		string stats = fetchStatsOnEntities((*entity)[i]);
		if (!stats.empty()) {
			if (allStats.empty())
				allStats = stats;
			else
				allStats = allStats + " " + stats;
		}
	}

	return allStats;
}

void World::renderEntities(Entity *entity, SDL_Renderer *renderer)
{
	if (entity->renderBeforeChildren) {
		entity->render(renderer);
	}

	for (int i = 0; i < entity->numChildren(); i++) {
		renderEntities((*entity)[i], renderer);
	}

	if (!entity->renderBeforeChildren) {
		entity->render(renderer);
	}
}

void World::performCleanup()
{
    int i = 0;
    for(set<Entity*>::iterator it = this->entityGarbage.begin(); it != this->entityGarbage.end(); ++it) {
        Entity *entity =(*it);
        delete entity;
        i++;
    }
    this->entityGarbage.clear();
    if(i > 0) {
        cout << "removed " << i << " entities" << endl;
    }
}

void World::addSdlEventListener(Uint32 event, Entity *entity) 
{
	SDLEventListener listener;
	listener.entity = entity;
	listener.event = event;
	this->eventListeners.push_back(listener);
}

void World::removeSdlEventListener(Uint32 event, Entity *entity)
{
	for (list<SDLEventListener>::iterator it = eventListeners.begin(); it != eventListeners.end();) {
		if ((*it).event == event && (*it).entity == entity) {
			eventListeners.erase(it++);
		}
		else {
			++it;
		}
	}
}

void World::notifyEntities(SDL_Event *event)
{
	for (list<SDLEventListener>::iterator it = eventListeners.begin(); it != eventListeners.end(); it++) {
		if ((*it).event == event->type) {
			(*it).entity->onSdlEvent(event);
		}
	}
}

void World::markForDeletion(Entity *entity)
{
    this->entityGarbage.insert(entity);
}