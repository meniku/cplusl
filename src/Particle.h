#pragma once
#include "Sprite.h"
class Particle :
	public Sprite
{
public:
	Vector2<float> velocity;
	float lifeTime = 1; 
	float livedTime = 1;
	float startScale = 1;
	float midScale = 1;
	float endScale = 1;
	float startVelocity = 0.1f;

	void update(float deltaTime);
	void enable();

	Particle();
	virtual ~Particle();
};
