#pragma once
#include "Entity.h"

#include "Vector2.h"
#include "SDL2/SDL.h"
#include <list>
#include <set>

#include "Resources.h"
#include "LuaConfig.h"

struct SDLEventListener
{
	Entity *entity;
	Uint32 event;
};

class World :
	public Entity
{
private:
	list<SDLEventListener> eventListeners;
    set<Entity *> entityGarbage;
	SDL_Rect screenRect;

public:
	float timeSinceStart = 0;

	Resources *resources;
	SDL_Renderer *renderer;
	LuaConfig *luaConfig;

	Vector2<float> topLeft;		
	Vector2<float> bottomRight;

	void setScreenRect(int width, int height);
	int getScreenWidth() {
		return screenRect.w;
	}
	int getScreenHeight() {
		return screenRect.h;
	}
	void enableEntities(Entity *entity);
	void disableEntities(Entity *entity);
	void updateEntities(Entity *entity, float deltaTime);
	void performCleanup();
	void renderEntities(Entity *entity, SDL_Renderer *renderer);
	void notifyEntities(SDL_Event *event);
	string fetchStatsOnEntities(Entity *entity);
    
	
	Vector2<float> screenPointToWorldPoint(Vector2<int> screenPoint);
	Vector2<int> worldPointToScreenPoint(Vector2<float> worldPoint);

	float pixelToWorldUnits(int pixels);
	int worldUnitsToPixels(float units);

	World(Resources *resources, SDL_Renderer *renderer, LuaConfig *luaConfig);
	~World();

	void addSdlEventListener(Uint32 event, Entity *entity);
	void removeSdlEventListener(Uint32 event, Entity *entity);

	virtual void enable();
	virtual void update(float deltaTIme);
    void markForDeletion(Entity *entity);
	
};

