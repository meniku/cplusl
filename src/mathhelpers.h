#pragma once

#include <cstdlib>

#include "Vector2.h"

#include <math.h>

#define PI 3.14159f    
#define RAD_TO_DEG 180.0f / PI
#define DEG_TO_RAD PI / 180.0f

float lerp(float t, float a, float b);

float randomFloat(float a, float b);

bool intersectCircle(const Vector2<float> pos1, const float rad1, const Vector2<float> pos2, const float rad2);