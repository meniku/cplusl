#pragma once
#include "Sprite.h"

typedef enum { CIRCLE } PhysicalShape;
typedef enum { LEFT, RIGHT, TOP, BOTTOM } Edge;

class PhysicalSprite :
	public Sprite
{
private:
	bool _isEdgeHit;
	bool _isOffScreen = false;
	bool _wasOnScreen = false;

public:
	Vector2<float> velocity;
	float angularVelocity;
	bool stayInBounds;
	bool collisionEnabled = true;
	PhysicalShape physicalShape = CIRCLE;

	PhysicalSprite(string name);
	virtual ~PhysicalSprite();

	virtual void enable();
	virtual void update(float deltaTime);
	virtual void edgeHit(Edge edge);

	bool collidesWith(const PhysicalSprite & other);

public:
	bool isEdgeHit() const
	{
		return _isEdgeHit;
	}

	bool isOffScreen() const
	{
		return _isOffScreen;
	}

	bool wasOnScreen() const
	{
		return _wasOnScreen;
	}

};

