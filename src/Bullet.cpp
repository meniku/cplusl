#include "Bullet.h"
#include "Bullets.h"
#include "World.h"
#include "Player.h"
#include "GameScene.h"

Bullet::Bullet() : PhysicalSprite("Bullet")
{
}


Bullet::~Bullet()
{
}

void Bullet::disable()
{
	particles.stop();
    particles.disable();
}

void Bullet::enable()
{
 	particles.setParent(this);
    particles.enable();
	this->stayInBounds = true;
	particles.loadFromConfig("particles_bullet");
	this->loadSprite("Bullet.bmp");
	this->scale.x = 4;
	this->scale.y = 4;
	this->velocity.xy(0, 0);
	this->outOfRange = false;
	particles.start();

	player = (Player *) this->findParentWithName("GameScene")->getChildWithName("Player");
	enemies = this->findParentWithName("GameScene")->getChildWithName("Enemies");
}

void Bullet::update(float deltaTime) 
{
	PhysicalSprite::update(deltaTime);

	lifeTime -= deltaTime;

	if (lifeTime < 0.125) {
        if (lifeTime > 0) {
			this->scale = 8 * lifeTime;
        } else {
			this->scale.xy(0);
        }
	}
	particles.position = this->position;

	
	if (outOfRange && this->collidesWith(*player)) {
		bounceBackTarget(player);
		((Bullets*)this->getParent())->remove(this);
		return;
	}
	else {
		for (int i = 0; i < this->enemies->numChildren(); ) {
			Enemy *enemy = (Enemy *) (*this->enemies)[i];
			if (this->collidesWith(*enemy)) {
				bounceBackTarget(enemy);
				((Bullets*)this->getParent())->remove(this);
				enemy->hit(this->velocity.Normalized(), this->strength);
				return;
			}
			else {
				i++;
			}
		}
	}
}

void Bullet::bounceBackTarget(PhysicalSprite *entity)
{
	LuaConfig *luaConfig = this->getWorld()->luaConfig;
	float rebound = luaConfig->callFloat("cannon", "bullet_rebound", this->scale.x, this->velocity.Length());
	entity->velocity += (this->velocity.Normalized() * rebound);
}