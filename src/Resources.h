#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include "Vector2.h"
#include <unordered_map>
#include <string>

using namespace std;

class ResourceException : public std::exception {
};

struct TextureResource
{
	bool valid = false;
	Vector2<int> size;
	SDL_Texture *texture = NULL;
};

struct MusicResource
{
	bool valid = false;
	Mix_Music *music;
};

struct SoundResource
{
	bool valid = false;
	Mix_Chunk *sound;
};


class Resources
{
private: 
	SDL_Renderer *renderer;
	unordered_map<string, TextureResource> textures;
	unordered_map<string, MusicResource> musics;
	unordered_map<string, SoundResource> sounds;

public:
	Resources(SDL_Renderer *renderer);
	~Resources();
	void destroyAll();

	TextureResource getTexture(string path);
	MusicResource getMusic(string path);
	SoundResource getSound(string path);
};

