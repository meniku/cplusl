#include "ParticleSystem.h"

#include "LuaConfig.h"
#include "World.h"

ParticleSystem::ParticleSystem() : Entity("System")
{
	
}

ParticleSystem::~ParticleSystem()
{
}

void ParticleSystem::loadFromConfig(char c_key[])
{
	LuaConfig *luaConfig = this->getWorld()->luaConfig;
	this->loop = luaConfig->getBool(c_key, "loop");
	this->duration = luaConfig->getFloat(c_key, "duration");
	this->maxParticles = luaConfig->getFloat(c_key, "maxParticles");
	this->lifeTime = luaConfig->getFloat(c_key, "lifeTime");
	this->startScale = luaConfig->getFloat(c_key, "startScale");
	this->midScale = luaConfig->getFloat(c_key, "midScale");
	this->endScale = luaConfig->getFloat(c_key, "endScale");
	this->emitDelay = luaConfig->getFloat(c_key, "emitDelay");
	this->emitMinAngle = luaConfig->getFloat(c_key, "emitMinAngle") * DEG_TO_RAD;
	this->emitMaxAngle = luaConfig->getFloat(c_key, "emitMaxAngle") * DEG_TO_RAD;
	this->startVelocity = luaConfig->getFloat(c_key, "startVelocity");
	this->sprite = luaConfig->getString(c_key, "sprite");
	this->useOldestWhenNoMoreParticles = luaConfig->getBool(c_key, "useOldestWhenNoMoreParticles");
	
}

void ParticleSystem::disable()
{
    while(this->numChildren() > 0) {
        Particle *particle = (Particle*) (*this)[0];
        this->remove(particle);
    }
}

void ParticleSystem::enable()
{
    if(this->numChildren() > 0) {
        cerr << "WARNING: particle system was not disabled properly previously " << endl;
    }
    this->particles = (Particles*) findParentWithName("GameScene")->getChildWithName("Particles");
}

void ParticleSystem::start()
{
	this->running = true;
}

void ParticleSystem::stop()
{
	this->running = false;
}

void ParticleSystem::remove(Particle *particle) {
    particle->disable();
    particle->setParent(NULL);
    this->particles->pool.dealloc(particle);
}

void ParticleSystem::update(float deltaTime)
{
	//this->position = getParent()->position;

	if (running || blockedTime >= 0) {
		blockedTime -= deltaTime;
	}

	if (running) {
		int spawned = 0;
		while (blockedTime < 0 && spawned < this->maxParticles) {
			if (this->emit()) {
				blockedTime += this->emitDelay;
				spawned++;
			}
		}
		runningTime += deltaTime;

		if (runningTime > this->duration) {
			runningTime = 0;
			if (!loop) {
				stop();
			}
		}
	}
}

bool ParticleSystem::emit()
{
	Particle *particle = this->getFreeParticle();
	if (particle == NULL) {
		return false;
	}
	particle->loadSprite(this->sprite);
	particle->lifeTime = this->lifeTime;
	particle->livedTime = 0;
	particle->startScale = this->startScale;
	particle->midScale = this->midScale;
	particle->endScale = this->endScale;
	
	particle->velocity = Vector2<float>(randomFloat(this->emitMinAngle, this->emitMaxAngle)) * this->startVelocity;
	particle->position = this->position;
	particle->enable();

	return true;
}

Particle * ParticleSystem::getFreeParticle()
{
	if (numChildren() < this->maxParticles) {
		Particle *particle = this->particles->pool.alloc();
        if(particle) {
            particle->setParent(this);
            return particle;
        } else {
            return NULL;
        }
	}

	if (!useOldestWhenNoMoreParticles) {
		return NULL;
	}
	Particle *oldestParticle = NULL;
    for (int i = 0; i < numChildren(); i++) {
        Particle *particle = (Particle*) (*this)[0];
		if (oldestParticle == NULL || particle->livedTime > oldestParticle->livedTime) {
			oldestParticle = particle;
		}
	}
    
    if(!oldestParticle) {
        return NULL;
    }

	oldestParticle->disable();
	return oldestParticle;
}
