engine = {
	window_title = "C+L",
	window_width = 1280,
	window_height = 1024,
	fps = 60,
	max_fps = 200,
	vsync = true
}

player = {
	load_shoot_duration = 1,
	scale_x_offset = 0,
	scale_y_offset = 0
}
player.scale_x = function(shootingFactor, deltaTime) 
	shootingSpeed = 1 + shootingFactor * 24
	player.scale_x_offset = player.scale_x_offset + shootingSpeed * deltaTime
	shootingSize = 1 + shootingFactor * 1
	return 6 + shootingSize * math.sin(player.scale_x_offset * 4) * 0.15
end
player.scale_y = function(shootingFactor, deltaTime) 
	shootingSpeed = 1 + shootingFactor * 24
	player.scale_y_offset = player.scale_y_offset + shootingSpeed * deltaTime
	shootingSize = 1 + shootingFactor * 1
	return 6 + shootingSize * math.sin(1 + player.scale_y_offset * 4) * 0.15
end

particles_bounds = {
	loop = false,
	sprite = "Player.bmp",
	duration = 0.0,
	maxParticles = 10,
	lifeTime = 0.2,
	startScale = 0,
	midScale = 0.5,
	endScale = 0,
	emitDelay = 0,
	emitMinAngle = 0,
	emitMaxAngle = 180,
	startVelocity = 0.5,
	useOldestWhenNoMoreParticles = true
}

cannon = {
	angle_between_weapons = 45,
	max_weapons = 8
}
cannon.bullet_life_time = function(factor)
	return 1 + factor;
end
cannon.bullet_shoot_speed = function(factor)
	return 2 - factor;
end
cannon.rebound = function(factor)
	return 0.07 + 0.07 * factor * math.max(0.4, factor);
end
cannon.bullet_rebound = function(scale, speed) 
	return (scale / 20) * (speed / 4)
end

particles_bullet = {
	loop = true,
	sprite = "Bullet.bmp",
	duration = 1,
	maxParticles = 100,
	lifeTime = 0.15,
	startScale = 0.2,
	midScale = 1,
	endScale = 0,
	emitDelay = 0.03,
	emitMinAngle = -180,
	emitMaxAngle = 180,
	startVelocity = 0.25,
	useOldestWhenNoMoreParticles = false
}

particles_enemy = {
	loop = false,
	sprite = "Enemy.bmp",
	duration = 0.0,
	maxParticles = 15,
	lifeTime = 0.15,
	startScale = 0,
	midScale = 0.5,
	endScale = 0,
	emitDelay = 0,
	emitMinAngle = -60,
	emitMaxAngle = 60,
	startVelocity = 0.5,
	useOldestWhenNoMoreParticles = true
}

game_scene = {
	
}

game_scene.spawn_enemies = function(pastTime, level) 

	-- spawnEnemy arguments: posX, posY, scale, velX, velY, angularVel

	if level == 0 then
		spawnEnemy(-0.25, 0, 4, 0, 0, 0)
		spawnEnemy(0.25, 0, 4, 0, 0, 0)
		spawnWeapon(0, -1, 0, 0.5)
	else
		-- if level % 1 == 0  then 
			spawnWeapon(0, 1.0, 0, -0.125)
		-- end

		numEnemies = level * 2 + 1

		for i=0,numEnemies  do
			speedX = (0.1 + math.random() * 0.2)
			speedY = (0.1 + math.random() * 0.2)
			if math.random() > 0.5 then
				x = -1.1 + math.random() * 2.2 
				if math.random() > 0.5 then
					y = -0.9
				else 
					y = 0.9
				end
				angle = math.random() * 4
				scale = 3 + math.random() * 2
				spawnEnemy(x, y, scale, -x * speedX, -y * speedY, angle)
			else
				if math.random() > 0.5 then
					x = -1.1
				else 
					x = 1.1
				end
				y = -0.9 + math.random() * 1.8
				angle = math.random() * 4
				scale = 3 + math.random() * 2
				spawnEnemy(x, y, scale, -x * speedX, -y * speedY, angle) 
			end
		end
	end
end
