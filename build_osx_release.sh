# check http://stackoverflow.com/questions/9098926/can-i-include-dylib-s-in-my-executable

rm -rf osx_release
mkdir osx_release
cd osx_release
cmake -DCMAKE_BUILD_TYPE=RELEASE ..
make

bundle=CplusL_osx.app
frameworks=$bundle/Contents/Frameworks
exec=$bundle/Contents/MacOS/CplusL_osx

# Find Paths:
mkdir $frameworks
cp /usr/local/lib/liblua.5.3.dylib $frameworks
cp /usr/local/lib/libSDL2-2.0.0.dylib $frameworks
cp /usr/local/lib/libSDL2_mixer-2.0.0.dylib $frameworks

# Fix Paths for executable
install_name_tool -change /usr/local/lib/liblua.5.3.dylib @executable_path/../Frameworks/liblua.5.3.dylib $exec
install_name_tool -change /usr/local/lib/libSDL2-2.0.0.dylib @executable_path/../Frameworks/libSDL2-2.0.0.dylib $exec
install_name_tool -change /usr/local/lib/libSDL2_mixer-2.0.0.dylib @executable_path/../Frameworks/libSDL2_mixer-2.0.0.dylib $exec

# Fix Paths for Mixer
mixer=$frameworks/libSDL2_mixer-2.0.0.dylib
chmod 644 $mixer
install_name_tool -change /usr/local/lib/libSDL2-2.0.0.dylib @executable_path/../Frameworks/libSDL2-2.0.0.dylib $mixer
install_name_tool -change /usr/local/lib/libSDL2_mixer-2.0.0.dylib @executable_path/../Frameworks/libSDL2_mixer-2.0.0.dylib $mixer
chmod 444 $mixer

echo "bundle ready at: osx_release/$bundle, linked libraries: "
otool -L $exec