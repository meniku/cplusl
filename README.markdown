# C Plus L

*In C plus L you control a spaceship which can hold up to eight weapons to shoot the evil enemies. When an enemy hits you, you loose one weapon, but it still floats round so you can collect it again. Every shot will kick you back a little, so you're able to control your movement a bit. Try to survive as many enemy waves as possible.*

![Gameplay](http://nilspferd.net/cplusl/gameplay.gif  =320x280)

It is my first (more or less finished) C++ game. The engine contains a very basic OOP based entity hierarchy and uses the SDL's 2D Accelerated Rendering. LUA scripting can be used to hold configuration values and simple functions to calculate stuff. It contains a very basic particle system (which is configured in LUA) and a circle based collison system (could be easily extended though). 

The engine lacks many important features like matrix based transformations (so that entities inherit transformations from their parents) and has little flexibility due to it's non-component, but inheritance based approach.

I built this in a couple of days and as the game turned out beeing no fun I will stop working on it now. 

## Controls

You shoot by **clicking the left mouse button** and can charge your weapons by **holding the mouse**. 

## Download

[Windows](http://gamejolt.com/games/c-plus-l/78461/download/build/220895)
[OSX](http://gamejolt.com/games/c-plus-l/78461/download/build/220894)
[Source](https://bitbucket.org/meniku/cplusl/get/master.zip)

## Developing

Windows: 

* install Cmake
* create visual studio project: `mkdir visualstudio; cd visualstudio; cmake ..`
* open the generated project in `visualstudio/CplusL.sln` (and switch to `CplusL_win` build target and run)
* set CPlusL solution as active solution and build 

(NOTE: CLion should work as an alternative to visual studio, but I did not test that)

OSX: 

* install Cmake, Lua 5.3, SDL 2.0.3 and SDL Mixer via homebrew: `brew install cmake lua53 sdl2 sdl2_mixer`
* decide wether you want to use CLion (a), Xcode (b) or build on the command line (c)
* a) In CLion just open the CMakeLists.txt as Project
* b) run `mkdir xcode; cd xcode; cmake -G Xcode ..` to generate XCode Project (after opening switch to `CplusL_osx` build target and run)
* c) run `mkdir build; cd build; cmake .. && make` to build directly on the command line
* NOTE: maybe the paths for the libraries don't match your system, go to CPLusL/CmakeLists.txt and alter them

## Building a Release Version

Windows:

* Follow all the steps under "Developing" to setup your visual studio project and just create a release build

OSX:

* install Cmake, Lua 5.3, SDL 2.0.3 and SDL Mixer via homebrew: `brew install cmake lua53 sdl2 sdl2_mixer`
* run `./build_osx_release.sh`